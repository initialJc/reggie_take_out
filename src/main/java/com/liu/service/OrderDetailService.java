package com.liu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.liu.entity.OrderDetail;

public interface OrderDetailService extends IService<OrderDetail> {
}
