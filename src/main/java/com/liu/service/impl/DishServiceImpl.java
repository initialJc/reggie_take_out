package com.liu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.liu.common.CustomException;
import com.liu.dto.DishDto;
import com.liu.entity.Dish;
import com.liu.entity.DishFlavor;
import com.liu.mapper.DishMapper;
import com.liu.service.DishFlavorService;
import com.liu.service.DishService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements DishService {

    @Resource
    private DishFlavorService dishFlavorService;

    /**
     * 新增菜品
     *
     * @param dishDto
     */
    @Override
    @Transactional
    public void saveDishWithFlavor(DishDto dishDto) {

        //保存菜品的基本信息到菜品表dish
        this.save(dishDto);

        //获取菜品id
        Long dishId = dishDto.getId();
        //获取菜品口味列表
        List<DishFlavor> flavors = dishDto.getFlavors();

        //将dishId添加到flavors的dishId字段中
        flavors = flavors.stream().peek(item -> item.setDishId(dishId)).collect(Collectors.toList());

        //保存菜品口味到菜品口味表dish_flavor
        dishFlavorService.saveBatch(flavors);

    }

    /**
     * 根据id查询菜品及口味信息
     *
     * @param id
     * @return
     */
    @Override
    public DishDto getDishWithFlavor(Long id) {
        //查询菜品基本信息
        Dish dish = this.getById(id);

        //将dish对象拷贝到DishDto对象中
        DishDto dishDto = new DishDto();
        BeanUtils.copyProperties(dish, dishDto);

        //根据dishId查询菜品口味信息
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId, id);
        List<DishFlavor> flavors = dishFlavorService.list(queryWrapper);

        //将口味信息添加到DishDto对象中
        dishDto.setFlavors(flavors);

        return dishDto;
    }

    /**
     * 更新菜品信息
     * @param dishDto
     */
    @Transactional
    public void updateDishWithFlavor(DishDto dishDto) {
        //根据dishId修改对应的菜品
        this.updateById(dishDto);

        //根据dishId删除对应的口味信息
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId, dishDto.getId());
        dishFlavorService.remove(queryWrapper);

        //获取前端提交的菜品口味列表,但是缺少dishId
        List<DishFlavor> flavors = dishDto.getFlavors();

        //将dishId添加到flavors的dishId字段中
        flavors = flavors.stream().peek(item -> item.setDishId(dishDto.getId())).collect(Collectors.toList());

        //保存菜品口味到菜品口味表dish_flavor
        dishFlavorService.saveBatch(flavors);
    }

    /**
     * 删除菜品及对应口味信息
     * @param ids
     */
    @Transactional
    public void removeWithFlavor(List<Long> ids) {
        //删除对应菜品
        LambdaQueryWrapper<Dish> dishQueryWrapper = new LambdaQueryWrapper<>();
        dishQueryWrapper.in(Dish::getId, ids);
        dishQueryWrapper.eq(Dish::getStatus, 1);
        if (this.count(dishQueryWrapper) > 0){
            throw new CustomException("菜品正在售卖中,不能删除");
        }
        this.removeByIds(ids);

        //删除对应菜品的口味信息
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper.in(DishFlavor::getDishId, ids);

        if (dishFlavorService.count(queryWrapper) > 0){
            dishFlavorService.remove(queryWrapper);
        }

    }
}
