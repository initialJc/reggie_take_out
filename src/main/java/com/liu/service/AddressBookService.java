package com.liu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.liu.entity.AddressBook;

public interface AddressBookService extends IService<AddressBook> {

}
