package com.liu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.liu.entity.Employee;

public interface EmployeeService extends IService<Employee> {

}
