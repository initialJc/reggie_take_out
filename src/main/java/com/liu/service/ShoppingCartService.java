package com.liu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.liu.entity.ShoppingCart;

public interface ShoppingCartService extends IService<ShoppingCart> {
}
