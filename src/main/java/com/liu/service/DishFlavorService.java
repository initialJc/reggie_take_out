package com.liu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.liu.entity.DishFlavor;

public interface DishFlavorService extends IService<DishFlavor> {
}
