package com.liu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.liu.dto.DishDto;
import com.liu.entity.Dish;

import java.util.List;

public interface DishService extends IService<Dish> {
    void saveDishWithFlavor(DishDto dishDto);

    public DishDto getDishWithFlavor(Long id);

    public void updateDishWithFlavor(DishDto dishDto);

    void removeWithFlavor(List<Long> ids);
}
