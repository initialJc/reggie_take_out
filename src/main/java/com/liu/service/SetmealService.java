package com.liu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.liu.dto.SetmealDto;
import com.liu.entity.Setmeal;

import java.util.List;

public interface SetmealService extends IService<Setmeal> {

    void saveSetmeslWithDish(SetmealDto setmealDto);

    void removeWithDish(List<Long> ids);

    //通过id查询套餐信息和菜品信息
    SetmealDto getSetmealWithDish(Long id);
}
