package com.liu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.liu.entity.SetmealDish;

public interface SetmealDishService extends IService<SetmealDish> {
}
