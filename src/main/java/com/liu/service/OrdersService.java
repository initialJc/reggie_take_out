package com.liu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.liu.entity.Orders;

public interface OrdersService extends IService<Orders> {
    void submit(Orders orders);
}
