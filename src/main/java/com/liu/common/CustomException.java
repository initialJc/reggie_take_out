package com.liu.common;


/**
 * 自定义异常类，用于处理业务逻辑中的异常情况。
 */
public class CustomException extends RuntimeException {
    public CustomException(String message) {
        super(message);
    }

}
