package com.liu.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.liu.common.BaseContext;
import com.liu.common.R;
import com.liu.entity.ShoppingCart;
import com.liu.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/shoppingCart")
@Slf4j
public class ShoppingCartController {

    @Resource
    private ShoppingCartService ShoppingCartService;

    /**
     * 添加购物车
     *
     * @param shoppingCart
     * @return
     */
    @PostMapping("/add")
    public R<ShoppingCart> add(@RequestBody ShoppingCart shoppingCart) {

        //从session中获取用户id绑定购物车
        Long currentId = BaseContext.getCurrentId();
        shoppingCart.setUserId(currentId);

        Long dishId = shoppingCart.getDishId();
        LambdaQueryWrapper<ShoppingCart> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ShoppingCart::getUserId, currentId);

        //判断前端传过来的商品类型
        if (dishId != null) {
            //如果是菜品信息，则添加到购物车的是菜品信息
            lambdaQueryWrapper.eq(ShoppingCart::getDishId, dishId);
        } else {
            //如果是套餐信息，则添加到购物车的是套餐信息
            lambdaQueryWrapper.eq(ShoppingCart::getSetmealId, shoppingCart.getSetmealId());
        }

        //查询当前菜品或套餐是否已经在购物车中
        ShoppingCart cartServiceOne = ShoppingCartService.getOne(lambdaQueryWrapper);
        //判断添加的是否是同一个商品，如果是则更新数量，否则新增购物车记录
        if (cartServiceOne != null) {
            //更新数量
            Integer number = cartServiceOne.getNumber();
            cartServiceOne.setNumber(number + 1);
            ShoppingCartService.updateById(cartServiceOne);
        } else {
            //新增购物车记录
            shoppingCart.setNumber(1);
            shoppingCart.setCreateTime(LocalDateTime.now());
            ShoppingCartService.save(shoppingCart);
            cartServiceOne = shoppingCart;
        }

        return R.success(cartServiceOne);
    }

    /**
     * 查询购物车
     *
     * @return
     */
    @GetMapping("/list")
    public R<List<ShoppingCart>> list() {

        //从数据库查询当前用户的购物车信息
        Long currentId = BaseContext.getCurrentId();
        LambdaQueryWrapper<ShoppingCart> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ShoppingCart::getUserId, currentId);
        lambdaQueryWrapper.orderByAsc(ShoppingCart::getCreateTime);

        List<ShoppingCart> list = ShoppingCartService.list(lambdaQueryWrapper);
        return R.success(list);
    }

    /**
     * 删除购物车中的商品
     *
     * @param shoppingCart
     * @return
     */
    @PostMapping("/sub")
    public R<String> sub(@RequestBody ShoppingCart shoppingCart) {
        //前端传过来的只有dishId/setmealId，根据dishId/setmealId查询数据库
        Long dishId = shoppingCart.getDishId();
        LambdaQueryWrapper<ShoppingCart> lambdaQueryWrapper = new LambdaQueryWrapper<>();

        //判断前端传过来的商品类型
        if (dishId != null) {
            lambdaQueryWrapper.eq(ShoppingCart::getDishId, dishId);
        } else {
            lambdaQueryWrapper.eq(ShoppingCart::getSetmealId, shoppingCart.getSetmealId());
        }

        shoppingCart = ShoppingCartService.getOne(lambdaQueryWrapper);
        Integer cartNumber = shoppingCart.getNumber();
        if (cartNumber != null) {
            //如果购物车中数量为1，则删除该购物车记录
            if (--cartNumber == 0) {
                ShoppingCartService.removeById(shoppingCart);
                return R.success("删除成功");
            }
            shoppingCart.setNumber(cartNumber);
            ShoppingCartService.updateById(shoppingCart);
        }

        return R.success("删除成功");

    }

    /**
     * 清空购物车
     *
     * @return
     */
    @DeleteMapping("/clean")
    public R<String> clean() {

        //查询当前用户的购物车信息
        LambdaQueryWrapper<ShoppingCart> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ShoppingCart::getUserId, BaseContext.getCurrentId());

        //删除当前用户的所有购物车记录
        ShoppingCartService.remove(lambdaQueryWrapper);

        return R.success("清空购物车成功");
    }
}
